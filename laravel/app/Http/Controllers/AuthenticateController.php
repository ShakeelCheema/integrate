<?php

namespace App\Http\Controllers;

use App\Models\Authenticate;
use App\Http\Requests\StoreAuthenticateRequest;
use App\Http\Requests\UpdateAuthenticateRequest;

use Illuminate\Http\Request;
use App\Helpers\ResponseHelper;
use App\Http\Controllers\HelperController;
// use Validator;
use http\Exception;
use DB;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Validator;



class AuthenticateController extends Controller
{
    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */

    // public function login (Request $request) {
    //     // $validator = Validator::make($request->all(), [
    //     //     'email' => 'required|string|email|max:255',
    //     //     'password' => 'required|string|min:6|confirmed',
    //     // ]);
    //     // if ($validator->fails())
    //     // {
    //     //     return response(['errors'=>$validator->errors()->all()], 422);

    //     // }
    //     // return $request->all();
    //     $user = User::where('email', $request->email)->first();
    //     if ($user) {
    //         if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
    //             return 'heelo';
    //             $token = $user->createToken('pat');
    //             $response = ['token' => $token];
    //             return ResponseHelper::buildResponse(true, $response, 'Login_Success', 200);
    //         } else {
    //             $response = "Password mismatch";
    //             return response($response, 422);
    //         }
    //     } else {
    //         $response = 'User does not exist';
    //         return ResponseHelper::buildResponse(true, $response, 'Login_Failed', 422);
    //     }
    // }

    public function login(Request $request){
        // $data = "asd";
        // return response()->json($data);
        // return ($request->all());
        if(ResponseController::checkResponse($request)){
            if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
                $auth = Auth::user();

                $user = User::find($auth->id);

                $token =  $user->createToken('Laravel Password Grant Client')->accessToken;

                // $user->fcm_token        = $request->fcm_token;

                // $role  = $user->getRoles();
                // $role = $role[0];

                $role = $user->roles()->first();

                $role_name = $role['name'];

                $user->update();


                $data = [
                    'success' => true,
                    // 'token'     => $token,
                    'user'   => $user,
                    'user_id' => $user->id,
                    'role'  => $role_name,


                    'message' => 'authorised',
                    'error_code' => null
                ];
                return response()->json($data);

                // return ResponseHelper:: buildResponse(true, $data, 'authorised', null);
            }
            else{
                return ResponseHelper::buildResponse(true, null, 'Unauthorised', '401');
            }
        }

        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){

            $user = Auth::user();
            return view('admin.dashboard');

            // return redirect('/admin/dashboard');
        }

    }

    public function logout(Request $request) {
        Auth::logout();
        return redirect('/');
      }



    public function register(Request $request)
    {

        $v = Validator::make($request->all(), [
            'email' => 'unique:users',
        ]);

        if ($v->fails()) {
            $failedRules = $v->failed();

            if (isset($failedRules['email']['Unique'])) {
                return ResponseHelper::buildResponse(false, null, 'Email is already taken', 'REGISTRATION_EMAIL_ALREADY_EXISTS');
            }
             else {
                return ResponseHelper::genericError();
            }
        }


        $user = new User();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->password = Hash::make($request->password);

        // return $request->role;
        // $user->fcm_token= $request->fcm_token;
        if($user->save()){
            // $success['token'] =  $user->createToken('MyApp')-> accessToken;
            // $type = (isset($user->role) && $user->role == 'customer') ? 'customer' : 'driver';
            // $type = (isset($type) && $type == 'customer') ? 'customer' : 'driver';
            // $type = 0;

            if($request->role == 'customer'){
                $type = 'customer';
            }
            elseif($request->role == 'driver'){
                $type = 'driver';
            }
            elseif($request->role == 'admin'){
                $type = 'admin';
            }

            $user->attachRole($type);

            $success['name'] =  $user->first_name;

            return ResponseHelper::buildResponse(true, $success, 'registered', 200);
        }
        else{
            return ResponseHelper::genericError();
        }



    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Authenticate  $authenticate
     * @return \Illuminate\Http\Response
     */
    public function show(Authenticate $authenticate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Authenticate  $authenticate
     * @return \Illuminate\Http\Response
     */
    public function edit(Authenticate $authenticate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Authenticate  $authenticate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Authenticate $authenticate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Authenticate  $authenticate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Authenticate $authenticate)
    {
        //
    }

    public function changePassword(Request $request)
    {

        $user = User::find($request->user_id);

        if (!(Hash::check($request->get('current_password'), $user->password))) {
            return ResponseHelper::buildResponse(false, null, 'Your current password does not matches with the password you provided. Please try again.', null);
        }

        if (strcmp($request->get('current_password'), $request->get('new_password')) == 0) {
            return ResponseHelper::buildResponse(false, null, 'New Password cannot be same as your current password. Please choose a different password', null);
        }

        if (isset($request->new_password)) {
            if ($request->new_password != $request->confirm_password) {
                return ResponseHelper::buildResponse(false, null, 'New Password And Confirm New Password Are Not Same', null);
            }
        }




        // $user->password = bcrypt($request->get('new_password'));

        $user->password = Hash::make($request->get('new_password'));
        $user->update();

        // $response['success'] = true;
        // $response['error'] = false;
        // $response['message'] = 'Success, Password changed successfully !';
        // return response()->json($response);


        return ResponseHelper::buildResponse(true, null, 'Success, Password changed successfully !', null);
        // return redirect()->back()->with("success","Password changed successfully !");

    }

    public function resetPassword(Request $request){

        // dd( $request->email);

        $user = User::where('email',$request->email)->first();



        if(ResponseController::checkResponse($request)){
            if($user){
                $user_mail = $user->email;

                $subject = 'Password Reset Request';

                $send_pass = HelperController::testEmail($subject,$user_mail);

                // return $send_pass['success'];

                if($send_pass['success'] == 1){
                    $user->password = Hash::make($send_pass['password']);
                    if($user->update()){
                        return ResponseHelper::buildResponse(true, null, 'New password has been sent to your email !', 200);
                    }
                }
                else{
                    return ResponseHelper::genericError();
                }

                return ResponseHelper::genericError();
            }
            else{
                return ResponseHelper::buildResponse(true, null, 'User not found against this email', 404);
            }
        }

        if($user){
            $user_mail = $user->email;

            $subject = 'Password Reset Request';

            $send_pass = HelperController::testEmail($subject,$user_mail);

            if($send_pass['success'] == 1){
                $user->password = Hash::make($send_pass['password']);
                if($user->update()){
                    return Redirect::back()->withSuccess('New password has been sent to your email');
                }
            }
            else{
                return Redirect::back()->withErrors('Password Reset Failed');
            }

            return Redirect::back()->withErrors('Password Reset Failed');
        }
        else{
            return Redirect::back()->withErrors('User not found against this email');
        }


    }

}
