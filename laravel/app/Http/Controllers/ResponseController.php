<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;


class ResponseController extends Controller
{
    public static function checkResponse(Request $request)
    {

        if($request->is("api*"))
        {
            return true;
        }

        return false;
    }
}
