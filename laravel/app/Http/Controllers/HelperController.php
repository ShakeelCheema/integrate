<?php

namespace App\Http\Controllers;

use App\Notifications\ResetPasswordNotification;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image as Image;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
use App\Notifications\UsersNotification;
use Config;
use Illuminate\Support\Facades\Notification;

class HelperController extends Controller
{
    public static function base64($image){

        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = mt_rand(1-9999999999).'.'.'png'; //mt_rand
        // \File::put(storage_path(). '/' . $imageName, base64_decode($image));

        // \File::put(public_path("/uploaded_files/".$path),$imageName);

        //  $request->file($filename)->move(public_path("/uploaded_files/".$path), $fileNameToStore);

        dd($imageName);
    }

    /**
      * Save image/file Start
      */

    public static function storeFile(Request $request, $path, $filename){

        if($request->hasFile($filename)) {

            // Get filename with extension
            $filenameWithExt = $request->file($filename)->getClientOriginalName();


            // Get just filename
            $fileOriginalname = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // remove extra speces from file
            $fileOriginalname =  str_replace(' ', '_',$fileOriginalname);

            // Get just ext
            $extension = $request->file($filename)->getClientOriginalExtension();
            //Filename to store
            $fileNameToStore = $fileOriginalname. time().'.'.$extension;
            // Upload Image

            $file = request()->file($filename);
            //$file->store('/', ['disk' => 'uploaded_files']);


            $request->file($filename)->move(public_path("/uploaded_files/".$path), $fileNameToStore);

            return 'uploaded_files/'.$path.'/'.$fileNameToStore;

        }
    }

    /**
      * Save image/file End
      */

    /**
      * Delete image/file Start
      */

    public static function deleteMedia($filepath){
        // dd($filepath);
        File::delete($filepath);
    }

    /**
      * Delete image/file End
      */

    /**
      * Send Email Start
      */

    // public static function testEmail($subject, $eMail)
    // {
    //   //   dd('a');
    //   // dd($eMail);
    //     // Instantiation and passing `true` enables exceptions
    //     $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
    //     // $code = mt_rand(10000000, 90000000);
    //     $code = substr(str_shuffle($permitted_chars), 0, 10);
    //     Notification::route('mail', $eMail)->notify(new ResetPasswordNotification($code));


    //       $msg = 'Message has been sent';
    //       return ['message' => $msg,  'success' =>1, 'password' => $code];


    //     // exit();
    //     $mail = new PHPMailer(true);

    //     try {
    //         //Server settings
    //       $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
    //       $mail->isSMTP();                                            // Send using SMTP
    //       $mail->Host       = 'a2plcpnl0383.prod.iad2.secureserver.net';                    // Set the SMTP server to send through
    //       $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
    //       $mail->Username   = 'info@ngo.quipsol.co';                     // SMTP username
    //       //   $mail->Password   = 'okGoogle@456';
    //       $mail->Password   = 'Abc123!';                             // SMTP password
    //       $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
    //       $mail->Port       = 465;                                    // TCP port to connect to

    //       //Recipients
    //       $mail->setFrom('info@ngo.quipsol.co', 'NGO');
    //     //   $mail->addAddress($eMail, 'Shakeel');     // Add a recipient
    //       $mail->addAddress($eMail);               // Name is optional

    //        //dd($name);

    //         // Content
    //         $mail->isHTML(true);                                  // Set email format to HTML
    //         $mail->Subject = $subject;
    //       //   $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
    //       $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
    //         // $code = mt_rand(10000000, 90000000);
    //         $code = substr(str_shuffle($permitted_chars), 0, 10);

    //         $mail->Body    = 'Your new password is: '.$code;
    //       //   $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    //         if($mail->send()){
    //             $msg = 'Message has been sent';
    //             return ['message' => $msg,  'success' =>1, 'password' => $code];
    //         }

    //     } catch (Exception $e) {
    //         $msg = "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    //         return ['message' => $msg, 'success' => 0, 'password' => $code];
    //     }
    // }

    /**
      * Send Email End
      */

}
