<?php

namespace App\Helpers;

class ResponseHelper {

    public static function buildResponse($success, $data, $message, $error_code) {
        $result = [
            'success' => $success,
            'data' => $data,
            'message' => $message,
            'error_code' => $error_code
        ];
        return response()->json($result);
    }

    public static function genericError() {
        $result = [
            'success' => false,
            'data' => null,
            'message' => 'A technical error occurred.',
            'error_code' => 'TECHNICAL_ERROR'
        ];
        return response()->json($result, 500);
    }

    public static function getKey() {
        $length = 32;
        $key = '';
        $codeAlphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $codeAlphabet.= 'abcdefghijklmnopqrstuvwxyz';
        $codeAlphabet.= '0123456789';
        $max = strlen($codeAlphabet);

        for ($i=0; $i < $length; $i++) {
            $key .= $codeAlphabet[random_int(0, $max-1)];
        }

        return $key;
    }
}
